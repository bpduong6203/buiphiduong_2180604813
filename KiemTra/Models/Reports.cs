﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Reports
    {
        [Key]
        public int ReportId { get; set; }
        public int AccountId { get; set; }
        public Accounts Account { get; set; }
        public int LogsId { get; set; }
        public Logs Log { get; set; }
        public int TransactionalId { get; set; }
        public Transactions Transaction { get; set; }
        public string ReportName { get; set; }
        public DateTime Reportdate { get; set; }
        public string Addtext { get; set; }
    }
}