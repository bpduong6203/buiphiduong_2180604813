﻿using KiemTra.Models;

public class Logs
{
    public int LogsId { get; set; }
    public int TransactionalId { get; set; }
    public Transactions Transaction { get; set; }
    public DateTime Logindate { get; set; }
    public DateTime Logoutdate { get; set; }
    public string Addtext { get; set; }
    public ICollection<Reports> Reports { get; set; }
}
