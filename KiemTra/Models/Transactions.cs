﻿using KiemTra.Models;
using System.ComponentModel.DataAnnotations;

public class Transactions
{

    [Key]
    public int TransactionalId { get; set; }
    public int EmployeeId { get; set; }
    public Employees Employee { get; set; }
    public int CustomerId { get; set; }
    public Customercs Customer { get; set; }
    public string Name { get; set; }
    public string Addtext { get; set; }
    public ICollection<Logs> Logs { get; set; }
    public ICollection<Reports> Reports { get; set; }
}
