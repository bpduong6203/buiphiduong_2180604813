﻿using Microsoft.EntityFrameworkCore;

namespace KiemTra.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Customercs> Customercs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>()
                .HasOne(a => a.Customer)
                .WithMany(c => c.Accounts)
                .HasForeignKey(a => a.CustomerId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Logs>()
                .HasOne(l => l.Transaction)
                .WithMany(t => t.Logs)
                .HasForeignKey(l => l.TransactionalId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Transaction)
                .WithMany(t => t.Reports)
                .HasForeignKey(r => r.TransactionalId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Reports>()
               .HasOne(r => r.Log)
               .WithMany(l => l.Reports)
               .HasForeignKey(r => r.LogsId)
               .OnDelete(DeleteBehavior.NoAction);
        }

    }
}
