﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public Customercs Customer { get; set; }
        public string AccountName { get; set; }
        public string AddText { get; set; }
    }
}