﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Customercs
    {
        [Key]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact_Address { get; set; }
        public string UserName { get; set; }
        public string Pass { get; set; }
        public string Addtext { get; set; }
        public ICollection<Accounts> Accounts { get; set; }
        public ICollection<Transactions> Transactions { get; set; }
    }
}