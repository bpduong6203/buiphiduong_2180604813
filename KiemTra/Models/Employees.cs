﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact_Address { get; set; }
        public string UserNameAndPass { get; set; }
        public string Addtext { get; set; }
        public ICollection<Transactions> Transactions { get; set; }
    }
}